<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('getStock', [\App\Http\Controllers\StockController::class, 'getStock']);
Route::get('submitBillEntry', [\App\Http\Controllers\TestController::class, 'submitBillEntry']);
Route::get('getBillTemplate', [\App\Http\Controllers\TestController::class, 'getBillTemplate']);
