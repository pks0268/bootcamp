<?php

namespace App\Interfaces;

interface StockInterface
{

    public function getStock($stockType);

}
