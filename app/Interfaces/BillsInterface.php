<?php

namespace App\Interfaces;

interface BillsInterface
{
    public function billType($billId);

    public function verifyRequestData($data);

    public function submitBill();

}
