<?php

namespace App\Interfaces;

interface SalaryTemplateInterface
{
    public function getSalaryMonth();
}
