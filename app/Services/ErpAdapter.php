<?php

namespace App\Services;

use App\Interfaces\StockInterface;

class ErpAdapter implements StockInterface
{

    public function getStock($stockType)
    {
        return $stockType . ' ERP Quantity';
    }
}
