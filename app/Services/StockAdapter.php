<?php

namespace App\Services;

use App\Interfaces\StockInterface;

class StockAdapter implements StockInterface
{


    public function getStock($stockType)
    {
        return $stockType . ' Stock Quantity';
    }
}
