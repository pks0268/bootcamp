<?php

namespace App\Http\BillTemplates;

use App\Interfaces\SalaryTemplateInterface;

class SalaryTemplate extends BillsTemplate implements SalaryTemplateInterface
{

    function getParty()
    {
        return "Salary Bill Parties";
    }

    function getAmount()
    {
        return 100000;
    }

    function getPurpose()
    {
        return "Salary Bill";

    }

    function getBillType()
    {
        return "Salary";
    }

    function getSalaryMonth() {
        return date("M");
    }
}
