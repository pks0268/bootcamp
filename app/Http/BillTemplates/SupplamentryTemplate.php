<?php

namespace App\Http\BillTemplates;

class SupplamentryTemplate extends BillsTemplate
{

    function getParty()
    {
        return "Supplementary Salary Bill Parties";
    }

    function getAmount()
    {
        return 20000;
    }

    function getPurpose()
    {
        return "Supplementary Salary Bill";

    }

    function getBillType()
    {
        return "Supplementary Salary";
    }
}
