<?php

namespace App\Http\BillTemplates;

class OtherTemplate extends BillsTemplate
{

    function getParty()
    {
        return "Others Bill Parties";
    }

    function getAmount()
    {
        return 5000;
    }

    function getPurpose()
    {
        return "Other Bill";

    }

    function getBillType()
    {
        return "Other";
    }
}
