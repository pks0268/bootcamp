<?php

namespace App\Http\BillTemplates;

abstract class BillsTemplate
{

    abstract function getParty();

    abstract function getAmount();

    abstract function getPurpose();

    abstract function getBillType();

    final function getBillsDetails() {
        $data["bill_type"] = $this->getBillType();
        $data["amount"] = $this->getAmount();
        $data["party"] = $this->getParty();
        $data["purpose"] = $this->getPurpose();
        if ($data["bill_type"] == "Salary"){
            $data['month'] = $this->getSalaryMonth();
        }
        return $data;
    }

}
