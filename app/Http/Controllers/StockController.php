<?php

namespace App\Http\Controllers;

use App\Interfaces\StockInterface;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function getStock(Request $request, StockInterface $stockInterface) {


        $stockType = $request->stockType;

        $res = $stockInterface->getStock($stockType);

        return $res;

    }
}
