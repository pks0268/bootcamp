<?php

namespace App\Http\Controllers;

use App\Http\BillTemplates\OtherTemplate;
use App\Http\BillTemplates\SalaryTemplate;
use App\Http\BillTemplates\SupplamentryTemplate;
use App\Interfaces\BillsInterface;
use App\Services\BillsEntry\OtherBills;
use App\Services\BillsEntry\SalaryBills;
use App\Services\BillsEntry\SupplementaryBills;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function submitBillEntry(Request $request) {
        $message = "";
        $type = $request->input('type');
        $data = [
          'type' => $type,
          'month' => 12,
          'year' => 2021,
          'amount' => 100000
        ];

        $bills = null;

        switch ($type){
            case(1):
                $bills = new SalaryBills();
                break;
            case(2):
                $bills = new OtherBills();
                break;
            case(3):
                $bills = new SupplementaryBills();
                break;
            default:
                $message = "Please select bill type";
                break;
        }

        if ($type) {
            $data['type_text'] = $bills->billType($type);
        } else {
            $data = $message;
        }
        return json_encode($data);
    }


    public function getBillTemplate(Request $request) {

        $data[] = "Create Bills Template";

        $message = "";
        $type = $request->input('type');
        switch ($type){
            case(1):
                $bill = new SalaryTemplate();
                break;
            case(2):
                $bill = new SupplamentryTemplate();
                break;
            case(3):
                $bill = new OtherTemplate();
                break;
            default:
                $message = "Please select bill type";
                break;
        }

        if ($type) {
            $data = $bill->getBillsDetails();
        } else {
            $data = $message;
        }

        return json_encode($data);
    }
}
