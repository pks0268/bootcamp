<?php

namespace App\Providers;

use App\Interfaces\StockInterface;
use App\Services\ErpAdapter;
use App\Services\StockAdapter;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StockInterface::class, function ($app) {
            switch ($app->make('config')->get('services.stock')) {
                case 'database':
                    return new StockAdapter();
                case 'erp':
                    return new ErpAdapter();
                default:
                    throw new \RuntimeException("Unknown Stock Service");
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
